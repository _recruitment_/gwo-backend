<?php

declare(strict_types=1);

namespace Recruitment\Tests\Entity;

use PHPUnit\Framework\TestCase;
use Recruitment\Entity\Product;

class ProductTest extends TestCase
{
    /**
     * @test
     * @expectedException \Recruitment\Entity\Exception\InvalidUnitPriceException
     */
    public function itThrowsExceptionForInvalidUnitPrice(): void
    {
        $product = new Product();
        $product->setUnitPrice(0);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function itThrowsExceptionForInvalidMinimumQuantity(): void
    {
        $product = new Product();
        $product->setMinimumQuantity(0);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function itThrowsExceptionForInvalidTax(): void
    {
        $product = new Product();
        $product->setTax(1);
    }

    /**
     * dodany
     * @test
     * @dataProvider getPossibleTaxes
     */
    public function itDoesNotThrowsExceptionForTax(float $tax): void
    {
        $product = new Product();
        $product->setTax($tax);

        $this->assertEquals($tax, $product->getTax());
    }

    /**
     * dodany
     * @test
     */
    public function itGetPriceGrossIfSet()
    {
        $product = (new Product())
            ->setUnitPrice(1000)
            ->setTax(23);

        $this->assertEquals(230, $product->getPriceGross());
    }

    /**
     * dodany
     * @test
     */
    public function itGetPriceGrossNotSet()
    {
        $product = (new Product())
            ->setUnitPrice(1000);

        $this->assertEquals(0, $product->getPriceGross());
    }

    public function getPossibleTaxes(): array
    {
        return [
            [0],
            [5],
            [8],
            [23],
        ];
    }


}

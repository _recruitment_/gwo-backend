<?php

declare(strict_types=1);

namespace Recruitment\Cart\Exception;

use LogicException;

class QuantityTooLowException extends LogicException
{

}

<?php

declare(strict_types=1);

namespace Recruitment\Cart;

use OutOfBoundsException;
use Recruitment\Entity\Order;
use Recruitment\Entity\Product;

class Cart
{
    /**
     * @var Item[]
     */
    private $items = [];

    /**
     * @return Item[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return float|int
     */
    public function getTotalPrice()
    {
        $result = 0.0;

        foreach ($this->items as $item) {
            $result += $item->getTotalPrice();
        }

        return $result;
    }

    /**
     * @param int $index
     *
     * @return Item
     * @throws OutOfBoundsException
     */
    public function getItem(int $index): Item
    {
        if (!isset($this->items[$index])) {
            throw new OutOfBoundsException(sprintf('There is no item index %s', $index));
        }

        return $this->items[$index];
    }

    /**
     * @param Product  $product
     * @param int|null $quantity
     *
     * @return $this
     */
    public function addProduct(Product $product, ?int $quantity = null)
    {
        if ($this->productExists($product)) { //TODO
            $this->setQuantity(
                $product,
                $this->items[$this->getProductIndex($product)]->getQuantity() + $quantity
            );

            return $this;
        }

        $this->items[] = is_null($quantity)
            ? new Item($product)
            : new Item($product, $quantity);

        return $this;
    }

    /**
     * @param Product $product
     *
     * @return $this
     */
    public function removeProduct(Product $product): Cart
    {
        if ($this->productExists($product)) {
            $itemIndex = $this->getProductIndex($product);

            array_splice($this->items, $itemIndex, 1);
        }

        return $this;
    }

    /**
     * @param Product $product
     * @param int     $quantity
     *
     * @return $this
     */
    public function setQuantity(Product $product, int $quantity): Cart
    {
        if ($this->productExists($product)) {
            $this->items[$this->getProductIndex($product)]->setQuantity($quantity);

            return $this;
        }

        return $this->addProduct($product, $quantity);
    }

    /**
     * @param int $orderId
     *
     * @return Order
     */
    public function checkout(int $orderId): Order
    {
        $order = new Order($orderId);
        $order->setItems($this->getItems());

        $this->items = [];

        return $order;
    }

    /**
     * @return Product[]
     */
    private function getProducts(): array
    {
        return array_map(function (Item $item) {
            return $item->getProduct();
        }, $this->getItems());
    }

    /**
     * @param Product $product
     *
     * @return false|int|string
     */
    private function getProductIndex(Product $product)
    {
        return array_search($product, $this->getProducts());
    }

    /**
     * @param Product $product
     *
     * @return bool
     */
    private function productExists(Product $product): bool
    {
        return is_int($this->getProductIndex($product));
    }

    /**
     * funkcja wylicza ile jednostek
     * @return float
     */
    public function getTotalPriceGross(): float
    {
        $result = 0.0;

        foreach ($this->items as $item) {
            $result += $item->getTotalPriceGross();
        }

        return $result;
    }
}

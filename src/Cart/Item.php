<?php

declare(strict_types=1);

namespace Recruitment\Cart;

use InvalidArgumentException;
use Recruitment\Cart\Exception\QuantityTooLowException;
use Recruitment\Entity\Product;

class Item
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var int
     */
    private $quantity = 1;

    public function __construct(Product $product, int $quantity = 1)
    {
        if ($this->isQuantityValid($product, $quantity)) {
            throw new InvalidArgumentException(
                sprintf("Minimum product quantity is %", $product->getMinimumQuantity())
            );
        }

        $this->product = $product;
        $this->quantity = $quantity;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     *
     * @return $this
     */
    public function setProduct(Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     *
     * @return $this
     */
    public function setQuantity(int $quantity): self
    {
        if ($this->isQuantityValid($this->getProduct(), $quantity)) {
            throw new QuantityTooLowException(
                sprintf("Minimum product quantity is %", $this->getProduct()->getMinimumQuantity())
            );
        }

        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalPrice()
    {
        return $this->product->getUnitPrice() * $this->getQuantity();
    }

    private function isQuantityValid(Product $product, int $quantity): bool
    {
        return $quantity < $product->getMinimumQuantity();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->getProduct()->getId();
    }

    /**
     * @return float
     */
    public function getTotalPriceGross(): float
    {
        return $this->product->getPriceGross() * $this->getQuantity();
    }

    /**
     * @param Item $item
     *
     * @return array
     */
    public function getView(): array
    {
        return [
            'id' => $this->getId(),
            'quantity' => $this->getQuantity(),
            'total_price' => $this->getTotalPrice(),
            'tax' => $this->getProduct()->getTax() . '%'
        ];
    }
}

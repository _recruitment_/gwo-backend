<?php

declare(strict_types=1);

namespace Recruitment\Entity;

use InvalidArgumentException;
use Recruitment\Entity\Exception\InvalidUnitPriceException;

class Product
{
    const DEFAULT_MINIMUM_QUANTITY = 1;
    const DEFAULT_PERCENTAGE_TAX = 0;

    const ALLOWED_PERCENTAGE_TAXES = [
        self::DEFAULT_PERCENTAGE_TAX,
        5,
        8,
        23,
    ];

    /**
     * @var int
     */
    private $id;

    /**
     * @var float
     */
    private $unitPrice;

    /**
     * @var int
     */
    private $minimumQuantity = self::DEFAULT_MINIMUM_QUANTITY;

    /**
     * @var float
     */
    private $tax = self::DEFAULT_PERCENTAGE_TAX;

    /**
     * @return float
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @param float $unitPrice
     */
    public function setUnitPrice(float $unitPrice)
    {
        if ($unitPrice <= 0) {
            throw new InvalidUnitPriceException();
        }

        $this->unitPrice = $unitPrice;

        return $this;
    }

    /**
     * @return int
     */
    public function getMinimumQuantity(): int
    {
        return $this->minimumQuantity;
    }

    /**
     * @param int $minimumQuantity
     *
     * @return $this
     */
    public function setMinimumQuantity(int $minimumQuantity): self
    {
        if ($minimumQuantity < self::DEFAULT_MINIMUM_QUANTITY) {
            throw new InvalidArgumentException(
                sprintf("Minimum quantity must not be lower than %s", self::DEFAULT_MINIMUM_QUANTITY)
            );
        }

        $this->minimumQuantity = $minimumQuantity;

        return $this;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getTax(): float
    {
        return $this->tax;
    }

    /**
     * @param float $tax
     *
     * @return $this
     */
    public function setTax(float $tax): self
    {
        if (!in_array($tax, self::ALLOWED_PERCENTAGE_TAXES)) {
            throw  new InvalidArgumentException(sprintf("Tax %s\%% is not allowed", $tax));
        }

        $this->tax = $tax;

        return $this;
    }

    /**
     * @return float
     */
    public function getPriceGross(): float
    {
        return $this->getUnitPrice() * $this->getTax() / 100;
    }
}

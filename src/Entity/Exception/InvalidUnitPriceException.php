<?php

declare(strict_types=1);

namespace Recruitment\Entity\Exception;

use LogicException;

class InvalidUnitPriceException extends LogicException
{

}

<?php

declare(strict_types=1);

namespace Recruitment\Entity;

use Recruitment\Cart\Item;

class Order
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Item[]
     */
    private $items = [];

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getDataForView(): array
    {
        return [
            'id' => $this->getId(),
            'items' => $this->getItemsView(),
            'total_price' => $this->getItemsTotalPrice(),
        ];
    }

    /**
     * @return array
     */
    public function getItemsView(): array
    {
        return array_map(function (Item $item) {
            return $item->getView();
        }, $this->getItems());
    }

    /**
     * @return float
     */
    public function getItemsTotalPrice(): float
    {
        $totalPrice = 0.0;
        foreach ($this->getItems() as $item) {
            $totalPrice += $item->getTotalPrice();
        }

        return $totalPrice;
    }

    /**
     * @param Item[] $items
     *
     * @return $this
     */
    public function setItems(array $items): Order
    {
        $this->items = $items;

        return $this;
    }

    /**
     * @return Item[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
